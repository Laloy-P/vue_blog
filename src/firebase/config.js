import firebase from 'firebase/app'
import 'firebase/firestore'


const firebaseConfig = {
  apiKey: "AIzaSyAaLVvVKcwF5EgcMcsvbrW7Pj_bJEW_Gw8",
  authDomain: "vue-firebase-80858.firebaseapp.com",
  projectId: "vue-firebase-80858",
  storageBucket: "vue-firebase-80858.appspot.com",
  messagingSenderId: "102836255334",
  appId: "1:102836255334:web:4328427514df96ea638ba7"
};


//init firebase
firebase.initializeApp(firebaseConfig)

//init firestore service 
const projectFirestore = firebase.firestore()

export { projectFirestore }